module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      ["nativewind/babel"],

      [
        "module-resolver",
        {
          alias: {
            components: "./app/components",
            navigation: "./app/navigation",
            screens: "./app/screens",
            services: "./app/services",
            stores: "./app/stores",
            types: "./app/types",
            utilities: "./app/utilities",
            hooks: "./app/hooks",
          },
          extensions: [".js", ".jsx", ".ts", ".tsx"],
        },
      ],
    ],
  };
};

