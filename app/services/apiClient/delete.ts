import axios from "axios";
import userStore from "stores/user";
import IBaseResponse, { IReturnData } from "types/response/base/IBaseResponse";

const apiDeleteClient = async <T>(url: string, token?: string | null): Promise<IReturnData<T>> => {
  let returnData = { error: false, data: null } as IReturnData<T>;
  try {
    const result = await axios
      .create({
        headers:
          token != null
            ? {
                Authorization: token ? "Bearer " + token : "Bearer " + userStore.getState().access_token,
              }
            : {},
      })
      .delete<IBaseResponse<T>>(url);

    if (result.status == 200) {
      returnData.error = false;
      returnData.data = result.data.data;
    }
  } catch (error) {
    console.error(error);
    returnData.error = true;
  }
  return returnData;
};

export default apiDeleteClient;
