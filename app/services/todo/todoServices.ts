import apiDeleteClient from "services/apiClient/delete";
import apiGetClient from "services/apiClient/get";
import apiPostClient from "services/apiClient/post";
import apiPutClient from "services/apiClient/put";
import ITodo from "types/response/todo/ITodo";
import Endpoints from "utilities/enums/Endpoint";

const getTodos = async () => {
  const response = await apiGetClient<ITodo[]>(Endpoints.TODO.LIST, null);
  return response;
};

const createTodo = async (title: string, handleAfterFetch: () => void, handleFinally: () => void) => {
  const response = await apiPostClient<ITodo>(
    Endpoints.TODO.CREATE,
    {
      data: {
        title: title,
        done: false,
      },
    },
    null,
  );
  if (response.error) {
    console.log("Có lỗi xảy ra");
  } else {
    handleAfterFetch();
  }
  handleFinally();
};

const toggleTodoStatus = async (id: string, status: boolean, handleAfterFetch: () => void) => {
  const response = await apiPutClient<ITodo>(
    Endpoints.TODO.EDIT + id,
    {
      data: {
        done: status,
      },
    },
    null,
  );
  if (response.error) {
    console.log("Có lỗi xảy ra");
  }
  handleAfterFetch();
};

const deleteTodo = async (id: string, handleAfterFetch: () => void) => {
  const response = await apiDeleteClient<ITodo>(Endpoints.TODO.EDIT + id, null);
  if (response.error) {
    console.log("Có lỗi xảy ra");
  }
  handleAfterFetch();
};

export { getTodos, createTodo, toggleTodoStatus, deleteTodo };
