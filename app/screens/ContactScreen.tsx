import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "navigation/StackNavigator";
import React from "react";
import { Text, View } from "react-native";

type Props = NativeStackScreenProps<RootStackParamList, "ContactScreen">;

function Contact(props: Props) {
  return (
    <View className="items-center justify-center flex-1 text-center">
      <View className="flex flex-col items-center justify-between h-28 w-80">
        <Text className="text-lg">This is the contact screen</Text>
      </View>
    </View>
  );
}

export default Contact;
