import { NativeStackScreenProps } from "@react-navigation/native-stack";
import TodoCard from "components/card/TodoCard";
import useTodoList from "hooks/useTodo";
import { RootStackParamList } from "navigation/StackNavigator";
import React from "react";
import { ActivityIndicator, Button, FlatList, Text, TextInput, View } from "react-native";
import { createTodo, deleteTodo, toggleTodoStatus } from "services/todo/todoServices";
import ITodo from "types/response/todo/ITodo";

type Props = NativeStackScreenProps<RootStackParamList, "TodosScreen">;

const ListView = (props: Props) => {
  const { isLoading, todo, data, setTodo, setLoading } = useTodoList();

  const renderTodoCards = (props: { item: ITodo }) => {
    const item = props.item;
    return (
      <TodoCard
        title={item.attributes.title}
        status={item.attributes.done}
        handleDelete={() => deleteTodo(item.id, () => setLoading(true))}
        handleToggle={() => toggleTodoStatus(item.id, !item.attributes.done, () => setLoading(true))}
      />
    );
  };

  return (
    <View className="w-full h-full">
      <View className="flex flex-col gap-2 p-4">
        <Text className="font-medium text-center text-gray-700">Todos list</Text>
        <View className="flex-row items-center">
          <TextInput
            placeholder="Add new todo"
            className="flex flex-1 p-1 bg-white border rounded-lg"
            onChangeText={(text: string) => setTodo(text)}
            value={todo}
          />
          <View className="p-1" />
          <Button
            onPress={() =>
              createTodo(
                todo,
                () => setTodo(""),
                () => setLoading(true),
              )
            }
            title="Add Todo"
            disabled={todo === ""}
            color="black"
          />
        </View>
        <View className="min-h-[100px] flex flex-1 justify-center">
          {isLoading ? (
            <ActivityIndicator />
          ) : (
            <FlatList data={data} keyExtractor={(item) => item.id} renderItem={renderTodoCards} />
          )}
        </View>
      </View>
    </View>
  );
};

export default ListView;
