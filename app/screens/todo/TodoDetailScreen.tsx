import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "app/navigation/StackNavigator";
import React from "react";
import { Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

type Props = NativeStackScreenProps<RootStackParamList, "TodoDetailScreen">;

function TodoDetail(props: Props) {
  return (
    <SafeAreaView>
      <View className="items-center justify-center flex-1 text-center">
        <View className="flex flex-col items-center justify-between h-28 w-80">
          <Text className="text-lg">This is the contact screen</Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default TodoDetail;
