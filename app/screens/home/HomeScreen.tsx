import { NativeStackScreenProps } from "@react-navigation/native-stack";
import CustomButton from "components/button/CustomButton";
import { RootStackParamList } from "navigation/StackNavigator";
import React from "react";
import { Text, View } from "react-native";

type Props = NativeStackScreenProps<RootStackParamList, "HomeScreen">;

function HomeScreen(props: Props) {
  return (
    <View className="items-center justify-center flex-1 text-center">
      <View className="flex flex-col items-center justify-between h-36 w-80">
        <Text className="text-lg">This is the home screen</Text>
        <CustomButton title="Go to Todos List" onPress={() => props.navigation.navigate("TodosScreen")} />
        <CustomButton title="Go to Login Screen" onPress={() => props.navigation.navigate("LoginScreen")} />
      </View>
    </View>
  );
}

export default HomeScreen;
