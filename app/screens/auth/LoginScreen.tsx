import { NativeStackScreenProps } from "@react-navigation/native-stack";
import LoadingModal from "components/modal/LoadingModal";
import { Formik } from "formik";
import { RootStackParamList } from "navigation/StackNavigator";
import React, { useState } from "react";
import { Button, Text, TextInput, View } from "react-native";
import { postLogIn } from "services/auth/logIn";
import userStore from "stores/user";
import { object, string } from "yup";

import Toast from "react-native-toast-message";

type Props = NativeStackScreenProps<RootStackParamList, "LoginScreen">;

export default function Login(props: Props) {
  const [isLoading, setLoading] = useState(false);
  return (
    <View className="flex flex-col p-4">
      <Formik
        validationSchema={object({
          phone: string().required("Vui lòng nhập số điện thoại111!"),
          password: string().required("Vui lòng nhập mật khẩu!"),
        })}
        initialValues={{
          phone: "",
          password: "",
        }}
        onSubmit={async (values: { phone: string; password: string }) => {
          setLoading(true);
          const res = await postLogIn(values.phone, values.password);

          if (res.error === false) {
            userStore.getState().saveAccessToken(res.data.access);
            setLoading(false);
            props.navigation.navigate("HomeScreen");
          } else {
            Toast.show({
              position: "top",
              type: "error",
              text1: "Thất bại",
              text2: "Thử đăng nhập: 🧊 phone: 0933168844 - pass: 0933168844 🐳",
            });
            setLoading(false);
          }
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
          <View className="flex flex-col">
            <View className="flex flex-col gap-2">
              <TextInput
                placeholder="Phone number"
                className="p-1 border-b border-gray-300"
                onChangeText={handleChange("phone")}
                onBlur={handleBlur("phone")}
                value={values.phone}
                keyboardType="phone-pad"
              />
              {errors.phone && touched.phone && <Text className="px-2 text-red-600">{errors.phone}</Text>}
              <TextInput
                placeholder="Password"
                className="p-1 border-b border-gray-300"
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                value={values.password}
                secureTextEntry
              />
              {errors.password && touched.password && <Text className="px-2 text-red-600">{errors.password}</Text>}
            </View>
            <View className="pt-8">
              <Button onPress={() => handleSubmit()} title="Submit" />
            </View>
          </View>
        )}
      </Formik>
      <LoadingModal visible={isLoading} />
    </View>
  );
}
