import { NativeStackScreenProps } from "@react-navigation/native-stack";
import LoadingModal from "components/modal/LoadingModal";
import { Formik } from "formik";
import { RootStackParamList } from "navigation/StackNavigator";
import React, { useState } from "react";
import { Button, TextInput, View } from "react-native";
import userStore from "stores/user";
import { object, string } from "yup";
import { postLogIn } from "../services/auth/logIn";

type Props = NativeStackScreenProps<RootStackParamList, "LoginScreen">;

export default function Login(props: Props) {
  const [isLoading, setLoading] = useState(false);
  return (
    <View className="flex flex-col p-4">
      <Formik
        validationSchema={object({
          phone: string().required("Vui lòng nhập tên đăng nhập!"),
          password: string().required("Vui lòng nhập mật khẩu!"),
        })}
        initialValues={{
          phone: "",
          password: "",
        }}
        onSubmit={async (values: { phone: string; password: string }) => {
          setLoading(true);
          const res = await postLogIn(values.phone, values.password);

          if (res.error === false) {
            userStore.getState().saveAccessToken(res.data.access);
            setLoading(false);
            props.navigation.navigate("HomeScreen");
          } else {
            setLoading(false);
          }
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View className="flex flex-col">
            <View className="flex flex-col gap-2">
              <TextInput
                placeholder="Phone number"
                className="p-1 border-b border-gray-300"
                onChangeText={handleChange("phone")}
                onBlur={handleBlur("phone")}
                value={values.phone}
                keyboardType="phone-pad"
              />
              <TextInput
                placeholder="Password"
                className="p-1 border-b border-gray-300"
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                value={values.password}
                secureTextEntry
              />
            </View>
            <View className="pt-8">
              <Button onPress={() => handleSubmit()} title="Submit" />
            </View>
          </View>
        )}
      </Formik>
      <LoadingModal visible={isLoading} />
    </View>
  );
}
