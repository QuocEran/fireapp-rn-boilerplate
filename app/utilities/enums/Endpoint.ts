// External link (endpoints api)
const BASE_URL_API = "https://api-qr-code.mltechsoft.com/api/v1";
const BASE_URL_CMS = "https://cms-qr-code.mltechsoft.com/api";
const WEB_DOMAIN = "https://mltechsoft.com";

const Endpoints = {
  BASE_URL: BASE_URL_API,
  CMS_URL: BASE_URL_CMS,
  WEB_DOMAIN: WEB_DOMAIN,
  LAYOUT: { BASE: BASE_URL_CMS + "/admin-layout" },
  TODO: {
    LIST: BASE_URL_CMS + "/qr-code-home-apis",
    CREATE: BASE_URL_CMS + "/qr-code-home-apis",
    EDIT: BASE_URL_CMS + "/qr-code-home-apis/",
  },
  AUTH: {
    CHECK_ACCOUNT: BASE_URL_API + "/auth/check-account",
    SIGN_UP: BASE_URL_API + "/auth/sign/up",
    LOG_IN: BASE_URL_API + "/auth/sign/in",
    REFRESH_TOKEN: BASE_URL_API + "/api/v1/token/renew",
  },
  EVENT: {
    LOYALTY: BASE_URL_API + "/event/loyaltys",
    DETAIL: BASE_URL_API + "/event/detail/",
  },
};

export default Endpoints;
