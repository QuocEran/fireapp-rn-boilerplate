// Internal link (for routing page to page)

const PageUrls = {
  BASE: "/",
  HOME: {
    BASE: "/dashboard",
  },
  AUTH: {
    LOGIN: "/auth/login",
  },
  EXAMPLE: {
    BASE: "/example",
  },
};

export default PageUrls;
