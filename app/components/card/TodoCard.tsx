import { Entypo, Ionicons } from "@expo/vector-icons";
import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

const TodoCard = (props: { title: string; status: boolean; handleToggle: any; handleDelete: any }) => {
  return (
    <View className="flex flex-row items-center justify-between p-2 my-1">
      <TouchableOpacity onPress={props.handleToggle} className="flex flex-row items-center gap-2">
        {!props.status && <Entypo name="circle" size={24} className="w-24" color="black" />}
        {props.status && <Ionicons name="md-checkmark-circle" className="w-24" size={24} color="black" />}
        <Text>{props.title}</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={props.handleDelete}>
        <Ionicons name="trash-bin" size={24} color="black" />
      </TouchableOpacity>
    </View>
  );
};

export default TodoCard;
