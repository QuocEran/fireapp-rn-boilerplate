import { useEffect, useState } from "react";
import { getTodos } from "services/todo/todoServices";
import ITodo from "types/response/todo/ITodo";

const useTodoList = () => {
  // state
  const [isLoading, setLoading] = useState(true);
  const [todo, setTodo] = useState("");
  const [data, setData] = useState<ITodo[]>([]);

  // effect
  useEffect(() => {
    getTodos()
      .then((response) => {
        if (!response.error) {
          setData(response.data);
        } else {
          console.log("Có lỗi xảy ra");
        }
      })
      .finally(() => setLoading(false));
  }, [isLoading]);
  return {
    isLoading,
    todo,
    data,
    setTodo,
    setData,
    setLoading,
  };
};

export default useTodoList;
