import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from "screens/auth/LoginScreen";
import Contact from "screens/contact/ContactScreen";
import Home from "screens/home/HomeScreen";
import TodoDetail from "screens/todo/TodoDetailScreen";
import ListView from "screens/todo/TodosScreen";

type RootStackParamList = {
  HomeScreen: undefined;
  TodosScreen: undefined;
  LoginScreen: undefined;
  ContactScreen: undefined;
  TodoDetailScreen: { todo_id: string };
};

const Stack = createNativeStackNavigator<RootStackParamList>();
const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "black",
  },
  headerTintColor: "white",
  headerBackTitle: "Back",
};

const ContactStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="ContactScreen" component={Contact} />
    </Stack.Navigator>
  );
};

export default function MainStackNavigator() {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="HomeScreen" component={Home} />
      <Stack.Screen name="LoginScreen" component={Login} />
      <Stack.Screen name="TodosScreen" component={ListView} />
      <Stack.Screen name="TodoDetailScreen" component={TodoDetail} />
    </Stack.Navigator>
  );
}

export type { RootStackParamList };
export { ContactStackNavigator };
