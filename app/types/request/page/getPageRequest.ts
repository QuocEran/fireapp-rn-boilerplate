import BaseQueryRequest from "../base";

interface IGetPageRequest {}

class GetPageRequest extends BaseQueryRequest implements IGetPageRequest {
  public sort: string[] = ["id:desc"];
  constructor(getHomeOptions: Partial<IGetPageRequest>, paging: BaseQueryRequest) {
    super(paging);
    Object.assign(this, getHomeOptions);
  }
}

export default GetPageRequest;
