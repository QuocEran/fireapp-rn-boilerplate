import BaseQueryRequest from "../base";

interface IGetBlogRequest {
  sort: string[];
}

class GetBlogRequest extends BaseQueryRequest implements IGetBlogRequest {
  public sort: string[] = ["id:desc"];
  constructor(getHomeOptions: Partial<IGetBlogRequest>, paging: BaseQueryRequest) {
    super(paging);
    Object.assign(this, getHomeOptions);
  }
}

export default GetBlogRequest;
