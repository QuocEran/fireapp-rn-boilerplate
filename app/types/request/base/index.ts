class BaseQueryRequest {
  public "pagination[page]": number = 0;
  public "pagination[pageSize]": number = 10;
  public populate: string = "deep";

  constructor(params: Partial<BaseQueryRequest>) {
    Object.assign(this, params);
  }
}

export default BaseQueryRequest;
