import { IImageResponse } from "./IBaseResponse";

interface ILayoutAttributes {
  nav_bar: IAppLink[];
}

interface ILayoutResponse {
  id: number;
  attributes: ILayoutAttributes;
}

interface IAppLink {
  title: string;
  icon: IImageResponse;
  href: string;
  col: boolean;
  status: boolean;
  childs: ILink[];
}

interface ILink {
  id?: number;
  title: string;
  description: string;
  icon: IImageResponse;
  href: string;
  status: boolean;
  external: boolean;
}

export type { ILink, IAppLink, ILayoutAttributes };

export default ILayoutResponse;
