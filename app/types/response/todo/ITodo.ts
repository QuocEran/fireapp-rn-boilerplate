interface ITodo {
  id: string;
  attributes: {
    id: number;
    title: string;
    description: string;
    done: boolean;
  };
}

export default ITodo;
