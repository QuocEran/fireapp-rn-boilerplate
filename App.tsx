import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaView } from "react-native";
import Toast from "react-native-toast-message";
import BottomTabNavigator from "./app/navigation/TabNavigator";
import GlobalStyles from "./app/styles/GlobalStyles";
export default function App() {
  return (
    <SafeAreaView style={GlobalStyles.AndroidSafeArea}>
      <NavigationContainer>
        <BottomTabNavigator />
      </NavigationContainer>
      <Toast />
    </SafeAreaView>
  );
}
