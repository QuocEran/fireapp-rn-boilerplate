## 🤏🏻 Prerequisites

- Node.js (>= 16.8.0) and npm
- Yarn (>= 1.22.0)

## 🍺 Note:

1, 🧑🏻‍💻 Dev command:

```bash
yarn
yarn start -c
```

2, 🏗️ Build command:

```bash
yarn global add eas-cli
eas login
eas build:configure
eas build [-p android|ios|all] [-e <value>] [--local] [--output <value>] [--wait] [--clear-cache]
    [--auto-submit | --auto-submit-with-profile <value>] [-m <value>] [--json --non-interactive]
```

3. 🎭 Styling: Stylesheet && Tailwind (Nativewind for Native platform)

- 🚲 Stylesheet: The built-in StyleSheet approach allows you to easily decouple the styles outside of JSX -> use for complex styling components.
- 🚗 Tailwind: Write style in className -> style can be shared between platforms and will output their styles as CSS Stylesheet on web and Stylesheet for native (Notice!: some CSS props cannot use in Native).

## 📦 Used packages

| Name                                                                         | Type             |
| ---------------------------------------------------------------------------- | ---------------- |
| [Tailwindcss](https://tailwindcss.com)                                       | style            |
| [Nativewind](https://www.nativewind.dev)                                     | style            |
| [Prettier](https://prettier.io/docs/en/index.html)                           | format           |
| [Formik](https://formik.org/docs/guides/react-native)                        | form utils       |
| [Yup](https://github.com/jquense/yup)                                        | form validation  |
| [Axios](https://axios-http.com/vi/docs/intro)                                | http client      |
| [React navigation](https://reactnavigation.org/docs/getting-started)         | navigation       |
| [Zustand](https://github.com/pmndrs/zustand)                                 | state management |
| [Async storage](https://github.com/react-native-async-storage/async-storage) | local storage    |
| [Bottom tab-bar](https://reactnavigation.org/docs/tab-based-navigation)      | bottom tab       |
| [expo/vector-icons](https://icons.expo.fyi/)                                 | icons            |

## 🗄 Template structure

#### ⚠️ Naming

- folders, services & custom hooks -> camel case
- files in components, screens -> pascal case
- custom hooks, modules, enums,...: -> [...].ts
- screens + components: -> [...].tsx

```
.
├── README.md                 # README file
├── next.config.js            # Next JS configuration
├── public                    # Assets folder
│   └── png, svg,...          # Image used by default template
│
├── app
│   ├── components            # All app components, separate by modules, pages
│       ├── example           # Example components
│       │   └── Logo.tsx      # Logo components
│       │   └── ...           # others
│       │
│       └──  button           # custom button components
│       └──  ...              # other components
│
│   ├── hooks                 # Custom hooks - define client side bussiness logic
│   ├── navigation            # React Navigation components (Stack, Tab,...)
│   ├── screens               # Screens components
│   ├── services              # Separate logic modules, fetch api funcs,...(naming & structure same components)
│   ├── stores                # Zustand files (create store, logic,...)
│   ├── styles                # Global & modules StyleSheet files
│   ├── types                 # Interfaces, class, generic,... (naming & structure same components)
│   └── utilities             # Utility folder (enums, utils modules,...)
│
├── tailwind.config.js        # Tailwind CSS configuration
├── tsconfig.json             # TypeScript configuration
├── babel.config.js           # Babel configuration
├── metro.config.js           # Metro configuration
├── eas.json                  # Build configuration file for EAS CLI and services
└── ...                       # Other configuration files (prettier, ignore files,...)

```

## Commit Message

```
<type>(<scope>):<Jira ID> - <short summary>
  │       │             │
  │       │             └─⫸ Summary in present tense. Not capitalized. No period at the end.
  │       │
  │       └─⫸ Commit Scope: animations|bazel|benchpress|common|compiler|compiler-cli|core|
  │                          elements|forms|http|language-service|localize|platform-browser|
  │                          platform-browser-dynamic|platform-server|router|service-worker|
  │                          upgrade|zone.js|packaging|changelog|docs-infra|migrations|
  │                          devtools
  │
  └─⫸ Commit Type: build|ci|chore|docs|feat|fix|perf|refactor|revert|style|test
```

##### Type

Must be one of the following:

- **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- **ci**: Changes to our CI configuration files and scripts (examples: CircleCi, SauceLabs)
- **chore**: add something without touching production code (Eg: update npm dependencies)
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **revert**: Reverts a previous commit
- **style**: Changes that do not affect the meaning of the code (Eg: adding white-space, formatting, missing semi-colons, etc)
- **test**: Adding missing tests or correcting existing tests

The `<type>` and `<summary>` fields are mandatory, the `(<scope>)` field is optional.
